<?php

/**
 * @file
 * Administration functions for layouts.
 */

use Drupal\layout\Plugin\Core\Entity\Display;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\layout\Form\BlockInstanceForm;
use Drupal\layout\Ajax\OpenDialogCommand;


/**
 * Page callback: Presents a list of layouts.
 *
 * @return array
 *   An array as expected by drupal_render().
 *
 * @see layout_menu()
 */
function layout_page_list() {
  // Get list of layouts defined by enabled modules and themes.
  $layouts = layout_manager()->getDefinitions();

  $rows = array();
  $header = array(t('Name'), t('Source'));
  foreach ($layouts as $name => $layout) {
    $provider_info = system_get_info($layout['provider']['type'], $layout['provider']['provider']);

    // Build table columns for this row.
    $row = array();
    $row['name'] = l($layout['title'], 'admin/structure/templates/manage/' . $name);
    // Type can either be 'module' or 'theme'.
    $row['provider'] = t('@name @type', array('@name' => $provider_info['name'], '@type' => t($layout['provider']['type'])));

    $rows[] = $row;
  }

  $build = array();
  $build['table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
  );
  return $build;

  // Ensure the provider types are translatable. These do not need to run,
  // just inform the static code parser of these source strings.
  t('module');
  t('theme');
}

/**
 * Page callback: Demonstrates a layout template.
 *
 * @param string $key
 *   The key of the page layout being requested.
 *
 * @return array
 *   An array as expected by drupal_render().
 *
 * @see layout_menu()
 */
function layout_page_view($key) {
  $layout = layout_manager()->getDefinition($key);
  drupal_set_title(t('View template %name', array('%name' => $layout['title'])), PASS_THROUGH);

  // Render the layout in an admin context with region demonstrations.
  $instance = layout_manager()->createInstance($key, array());
  $regions = $instance->getRegions();
  foreach ($regions as $region => $info) {
    $regions[$region] = '<div class="layout-region-demonstration">' . check_plain($info['label']) . '</div>';
  }
  $build['demonstration'] = array(
    '#type' => 'markup',
    '#markup' => $instance->renderLayout(TRUE, $regions),
  );
  $build['#attached']['css'][] = drupal_get_path('module', 'layout') . '/layout.admin.css';
  return $build;
}

/**
 * Page callback: Presents list of displays.
 *
 * @see display_menu()
 */
function layout_master_list() {
  $controller = entity_list_controller('display');
  return $controller->render();
}

/**
 * Page callback: Presents the display editing form.
 *
 * @see display_menu()
 */
function layout_master_edit(Display $display) {
  drupal_set_title(t('<em>Edit layout</em> @label', array('@label' => $display->label())), PASS_THROUGH);
  return entity_get_form($display);
}

/**
 * Page callback: webservice handling RESTful(-ish) requests from the JS app.
 * Currently it only updates the *whole*
 *
 * @todo: convert this to use the routing system (currently we use
 * Backbone.emulateHTTP).
 * @todo: allow this to consume application/json-payloads (currently we need
 * Backbone.emulateJSON)
 * @todo: protect against CSRF. We should use a session-based token. This issue
 * will reoccur with all RESTful services, because  a per-request token (à la
 * form-token just is not RESTful).
 *
 * @param Drupal\layout\Plugin\Core\Entity\Display $display
 * @param null $a
 * @param null $b
 * @param null $c
 * @return int
 */
function layout_master_webservice(Display $display, $a=NULL, $b=NULL, $c=NULL) {
  if (isset($display->locked) && is_object($display->locked) && $display->locked->owner != $GLOBALS['user']->uid) {
    // @todo: currently menu_access_denied returns a status 200?
    // @todo: make sure JS client handles this properly.
    return MENU_ACCESS_DENIED;
  }
  // This is all evil, evil, evil - hacking this blindly bottom-up.
  $payload = isset($_POST['model']) ? drupal_json_decode($_POST['model']) : FALSE;
  $method = isset($_POST['_method']) ? $_POST['_method'] : FALSE;
  switch ($a) {
    // stores complete layout
    case 'layout':
      $blockInfo = array();
      // Set payload - this *obviously* needs to be validated.
      foreach ($payload['regions'] as $region) {
        foreach ($region['blockInstances'] as $blockInstance) {
          $block = $blockInstance['id'];
          $blockInfo['block.' . $block] = array(
            'region' => $region['id'],
            'weight' => $blockInstance['weight'] * 100,
            'label' => $blockInstance['label'],
          );
        }
      }
      $display->set('blockInfo', $blockInfo);
      // Store changes in TempStore.
      layout_master_cache_set($display);
      drupal_exit();
      break;
    // Update single region
    case 'region':
      $region_id = $b;
      die();
      break;
    // Update single block
    case 'block':
      $block_id = $b;
      break;
  }
  return ;
}


/**
 * Page to break lock on a display being edited.
 */
function layout_master_break_lock_confirm($form, &$form_state, Display $display) {
  $form_state['display'] = &$display;
  $form = array();
  if (empty($display->locked)) {
    $form['message']['#markup'] = t('There is no lock on display %name to break.', array('%name' => $display->get('id')));
    return $form;
  }

  $cancel = drupal_container()->get('request')->query->get('cancel');
  if (empty($cancel)) {
    $cancel = 'admin/structure/layouts/manage/' . $display->get('id') . '/edit';
  }

  $account = user_load($display->locked->owner);
  $form = confirm_form($form,
    t('Do you want to break the lock on display %name?',
      array('%name' => $display->get('label'))),
    $cancel,
    t('By breaking this lock, any unsaved changes made by !user will be lost.', array('!user' => theme('username', array('account' => $account)))),
    t('Break lock'),
    t('Cancel'));
  $form['actions']['submit']['#submit'][] = 'layout_master_break_lock_confirm_submit';
  return $form;
}

/**
 * Form submit handler to break_lock on a display.
 */
function layout_master_break_lock_confirm_submit(&$form, &$form_state) {
  $display = $form_state['display'];
  drupal_container()->get('user.tempstore')->get('layout')->delete($display->get('id'));
  $form_state['redirect'] = 'admin/structure/layouts/manage/' . $display->get('id') . '/edit';
  drupal_set_message(t('The lock has been broken and you may now edit this display.'));
}


/**
 * Page callback for BlockInstance Configuration Form
 *
 * THIS IS ONLY FOR DEMONSTRATION PURPOSES!
 *
 * @param Drupal\layout\Plugin\Core\Entity\Display $display
 * @param $region
 * @param $block_id
 * @return Drupal\Core\Ajax\AjaxResponse
 */
function layout_master_blockinstance_configure(Display $display, $block_id) {
  $response = new AjaxResponse();
  $form_state = array(
    'display' => $display,
    'block_id' => $block_id
  );
  if (!$display->getBlockInstanceInfo($block_id)) {
    return MENU_NOT_FOUND;
  }

  $form = drupal_build_form('layout_master_blockinstance_configure_form', $form_state);
  $html = drupal_render($form);
  $response->addCommand(new OpenDialogCommand( t('Configure %block_id in layout %name', array('%block_id' => $block_id, '%name' => $display->get('label'))), $html));
  return $response;
}

function layout_master_blockinstance_configure_form(array $form, array &$form_state) {
  $form_handler = new BlockInstanceForm();
  return $form_handler->build($form, $form_state);
}
