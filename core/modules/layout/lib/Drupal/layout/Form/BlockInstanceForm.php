<?php

/**
 * @file
 * Contains \Drupal\layout\Form\EditFieldForm.
 */

namespace Drupal\layout\Form;

/**
 * Example for a form loaded into a dialog.
 */
class BlockInstanceForm {
  /**
   * Display
   */
  protected $display;

  /**
   * BlockId
   */
  protected $blockId;


  /**
   * Builds a form for a single entity field.
   */
  public function build(array $form, array &$form_state) {
    $this->display = $form_state['display'];
    $this->blockId = $form_state['block_id'];
    $block_info = $this->display->getBlockInstanceInfo($this->blockId);

    $form = array();
    $form['error-placeholder'] = array(
      '#markup' => '<div class="error-placeholder"></div>',
    );
    $form['label'] = array(
      '#title' => t('Enter a label'),
      '#type' => 'textfield',
      '#default_value' => isset($block_info['label']) ? $block_info['label'] : NULL
    );
    $form['save'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
      '#ajax' => array(
        'dialog' => array(),
        'callback' => array($this, 'ajaxSave')
      ),
    );
    $form['cancel'] = array(
      '#type' => 'link',
      '#title' => t('Cancel'),
      '#href' => '#',
      '#attributes' => array(
        // This is a special class to which JavaScript assigns dialog closing
        // behavior.
        'class' => array('dialog-cancel'),
      ),
    );
    // Add validation and submission handlers.
    $form['#validate'][] = array($this, 'validate');
    $form['#submit'][] = array($this, 'submit');
    return $form;
  }

  function ajaxSave($form, $form_state) {
    // @todo: use proper AjaxResponse commands.
    if ($form_state['executed'] && $form_state['submitted']) {
      $commands = array();
      $commands[] = array('command'=>'layoutBlockReload', 'data' => array(
        'info' => $this->display->getBlockInstanceInfo($this->blockId),
        'id' => $this->blockId
      ));
      $commands[] = array('command'=>'remove', 'selector' => '#drupal-modal', 'data' => NULL);
      return array('#type' => 'ajax', '#commands' => $commands);
    }
    $commands = array();
    $errors = form_get_errors();
    if (count($errors)) {
      $commands[] = array(
        'command'=>'insert',
        'method' => 'replaceWith',
        'selector' => '#drupal-modal .error-placeholder',
        'data' => theme('status_messages', $errors)
      );
    }
    return array('#type' => 'ajax', '#commands' => $commands);
  }

  /**
   * Validates the form.
   */
  public function validate(array $form, array &$form_state) {
    $values = & $form_state['values'];
    if (strlen($values['label'])>10) {
      form_set_error('label', t('Please chose a shorter label (for the sake testing validation!)'));
    }
  }

  /**
   * Store the values by updating the TempStored Display.
   */
  public function submit(array $form, array &$form_state) {
    $values = $form_state['values'];
    // Update the instance info
    $this->display->setBlockInstanceInfo($this->blockId, array(
      'label'=>$values['label']
    ));
    // Store changes in TempStore.
    layout_master_cache_set($this->display);
  }
}
