<?php

/**
 * @file
 * Definition of Drupal\layout\Ajax\OpenDialogCommand
 *
 * @todo: this should be complemented by a CloseDialogCommand etc. and move to
 * Ajax/core.
 */

namespace Drupal\layout\Ajax;

use Drupal\Core\Ajax\InsertCommand;

class OpenDialogCommand extends InsertCommand {
  /**
   * Dialog title
   *
   * @var string
   */
  protected $title;

  /**
   * Constructs an InsertDialogCommand object.
   *
   * @param string $selector
   *   A CSS selector.
   * @param string $html
   *   String of HTML that will replace the matched element(s).
   * @param array $settings
   *   An array of JavaScript settings to be passed to any attached behaviors.
   */
  public function __construct($title, $html, array $settings = NULL) {
    $this->title = $title;
    $this->html = $html;
    $this->settings = $settings;
  }

  /**
   * Implements Drupal\Core\Ajax\CommandInterface:render().
   */
  public function render() {
    return array(
      'command' => 'insert',
      'method' => NULL,
      'title' => $this->title,
      'selector' => '#drupal-modal',
      'data' => $this->html,
      'settings' => $this->settings,
    );
  }
}
