<?php

/**
 * @file
 * Definition of Drupal\layout\DisplayFormController.
 */

namespace Drupal\layout;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityFormController;
use Drupal\layout\Plugin\Core\Entity\Display;

/**
 * Form controller for the display edit forms.
 */
class DisplayFormController extends EntityFormController {

  /**
   * Overrides Drupal\Core\Entity\EntityFormController::form().
   */
  public function form(array $form, array &$form_state, EntityInterface $display) {
    // Get list of layouts to be exposed for display layout selection.
    $layouts = drupal_container()->get('plugin.manager.layout')->getDefinitions();
    $layout_options = array();
    foreach ($layouts as $key => $layout) {
      $layout_options[$key] = $layout['title'];
    }
    $layout_keys = array_keys($layout_options);

    // Add default values to form_state['values'].
    if (!isset($form_state['values'])) {
      $form_state['values'] = array();
    }


    $form_state['values'] += array(
      'layout' => isset($display->layout) ? $display->layout : reset($layout_keys)
    );

    $locked = isset($display->locked) && is_object($display->locked) && $display->locked->owner != $GLOBALS['user']->uid;
    // Copied from ViewsEditFormController
    if ($locked) {
      $form['locked'] = array(
        '#type' => 'container',
        '#attributes' => array('class' => array('view-locked', 'messages', 'warning')),
        '#children' => t('This display is being edited by user !user, and is therefore locked from editing by others. This lock is @age old. Click here to <a href="@break">break this lock</a>.',
          // @todo: add callback to break lock.
          array(
            '!user' => theme('username', array('account' => user_load($display->locked->owner))),
            '@age' => format_interval(REQUEST_TIME - $display->locked->updated),
            '@break' => url('admin/structure/layouts/manage/' . $display->get('id') . '/break-lock')
          )
        ),
        '#weight' => -10,
      );
    }
    else {
      $message = t('* All changes are stored temporarily. Click Save to make your changes permanent. Click Cancel to discard your changes.');

      $form['changed'] = array(
        '#type' => 'container',
        '#attributes' => array('class' => array('display-changed', 'messages', 'warning')),
        '#children' => $message,
        '#weight' => -10,
      );
      if (empty($display->changed)) {
        $form['changed']['#attributes']['class'][] = 'js-hide';
      }
    }

    $form['layout'] = array(
      '#type' => 'select',
      '#title' => t('Template'),
      '#default_value' => $form_state['values']['layout'],
      '#options' => $layout_options,
      '#ajax' => array(
        'callback' => 'layout_ajax_block_placement_callback',
        'wrapper' => 'display-blocks',
        'method' => 'replace',
        'effect' => 'fade',
      ),
    );

    // To support the Ajax interaction, remap the display to the newly selected
    // layout. This will reorganize the blocks as appropriate.
    if (!isset($display->layout) || ($form_state['values']['layout'] != $display->layout)) {
      // @todo: clean this up - this is highly likely to be the wrong place
      // to alter the TempStore.
      // Figured out where this problem comes from form cache and tempstore
      // compete obviously - see also ViewEditFormController where they use
      // $form_state['no_cache'] = TRUE; but that again requires going through
      // views_ui_ajax_get_form instead of ajax_get_form.

      // But if i *don't* reload the blockInfo property of the  Display, it can
      // be "stale" and overwrite changes made via the webservice.
      $reloaded_display = layout_master_cache_load($display->id);
      // Need to copy selectively (because just setting $display = $reloaded_display
      // breaks other things).
      $display->set('blockInfo', $reloaded_display->get('blockInfo'));
      // Now remap.
      $layout = layout_manager()->createInstance($form_state['values']['layout'], array());
      $display->remapToLayout($layout);
      // Store changes in TempStore.
      layout_master_cache_set($display);
    }

    // Add block editing interface wrapper for Ajax operation.
    $form['blocks'] = array(
      '#prefix' => '<div id="display-blocks" class="layout-app">',
      '#suffix' => '</div>',
    );
    $form['blocks']['demonstration'] = $this->layoutDemonstration($display);
    return parent::form($form, $form_state, $display);
  }

  /**
   * Produces a render array demonstration form of the display.
   */
  private function layoutDemonstration(EntityInterface $display) {
    // Render the layout in an admin context with region demonstrations.
    $layout = layout_manager()->createInstance($display->layout, array());
    $build['demonstration'] = array(
      '#type' => 'markup',
      '#markup' => $layout->renderLayout(TRUE, array()),
    );
    // Add the backbone app.
    $build['#attached']['library'][] = array('layout', 'layout.admin');
    $locked = isset($display->locked) && is_object($display->locked) && $display->locked->owner != $GLOBALS['user']->uid;
    // Add the webservice URL and display id.
    $build['#attached']['js'][] =array(
      'data' => array(
        'layout' => array(
          'id' => $display->id,
          'webserviceURL' => url('admin/structure/layouts/manage/' . $display->id . '/webservice'),
          'locked' => $locked,
          'layoutData' => $display->exportGroupedByRegion()
        )
      ),
      'type' => 'setting',
    );

    return $build;
  }

  /**
   * Overrides Drupal\Core\Entity\EntityFormController::actions().
   */
  protected function actions(array $form, array &$form_state) {
    // Only includes a Save action for the entity, no direct Delete button.
    return array(
      'submit' => array(
        '#value' => t('Save'),
        '#validate' => array(
          array($this, 'validate'),
        ),
        '#submit' => array(
          array($this, 'submit'),
          array($this, 'save'),
        ),
      ),
      'cancel' => array(
        '#value' => t('Cancel'),
        '#submit' => array(
          array($this, 'cancel'),
        ),
      )
    );
  }

  /**
   * Overrides Drupal\Core\Entity\EntityFormController::save().
   */
  public function save(array $form, array &$form_state) {
    $display = $this->getEntity($form_state);
    $cached_display = layout_master_cache_load($display->id);
    // All changes are already in TempStore. So a save commit all changes.
    $cached_display->save();
    // Bust the TempStore. Remove the display form TempStore so it will be reloaded.
    drupal_container()->get('user.tempstore')->get('layout')->delete($cached_display->id);

    watchdog('display', 'Layout @label saved.', array('@label' => $display->label()), WATCHDOG_NOTICE);
    drupal_set_message(t('Layout %label saved.', array('%label' => $display->label())));
  }

  /**
   * Form submission handler for the 'cancel' action.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param array $form_state
   *   A reference to a keyed array containing the current state of the form.
   */
  public function cancel(array $form, array &$form_state) {
    // Remove this view from cache so edits will be lost.
    $display = $this->getEntity($form_state);
    $cached_display = layout_master_cache_load($display->id);
    drupal_container()->get('user.tempstore')->get('layout')->delete($cached_display->id);
    $form_state['redirect'] = 'admin/structure/layouts';
  }

}
