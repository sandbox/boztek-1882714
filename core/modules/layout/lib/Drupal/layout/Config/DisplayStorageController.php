<?php
/**
 * @file ConfigStorageController.php
 *
 * Definition of Drupal\layout\Config\DisplayStorageController.
 */

namespace Drupal\layout\Config;

use Drupal\Core\Config\Entity\ConfigStorageController;
use Drupal\Core\Entity\EntityInterface;
use Drupal\layout\Config\DisplayInterface;

/**
 * Storage controller for all Display entity types.
 */
class DisplayStorageController extends ConfigStorageController {
  public function getProperties(EntityInterface $entity) {
    if (!$entity instanceof DisplayInterface) {
      throw new \RuntimeException(sprintf('DisplayStorageController::getProperties can only work with objects implementing Drupal\\layout\\Config\\DisplayInterface, object of type "%s" was given.', get_class($entity)));
    }

    return $entity->export();
  }
}
