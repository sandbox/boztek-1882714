<?php

/**
 * @file
 * Definition of Drupal\layout\DisplayListController.
 */

namespace Drupal\layout;

use Drupal\Core\Config\Entity\ConfigEntityListController;
use Drupal\Core\Entity\EntityInterface;
use Drupal\layout\Plugin\Core\Entity\Display;

/**
 * Provides a listing of displays.
 */
class DisplayListController extends ConfigEntityListController {

  /**
   * Overrides Drupal\Core\Entity\EntityListController::getOperations().
   *
   * Only allow edit operation, not possible to delete.
   */
  public function getOperations(EntityInterface $entity) {
    $uri = $entity->uri();
    $operations['edit'] = array(
      'title' => t('Edit'),
      'href' => $uri['path'] . '/edit',
      'options' => $uri['options'],
      'weight' => 10,
    );
    return $operations;
  }

  /**
   * Overrides Drupal\Core\Entity\EntityListController::buildHeader();
   */
  public function buildHeader() {
    $row['label'] = t('Layout name');
    $row['applied'] = t('Applied to');
    $row['template'] = t('Template');
    $row['operations'] = t('Operations');
    return $row;
  }

  /**
   * Overrides Drupal\Core\Entity\EntityListController::buildRow();
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = check_plain($entity->label());
    // @todo: refer back to pages using this display.
    $row['applied'] = $entity->id() == 'front_master' ? t('All front end pages (master)') : t('All admin pages (admin master)');
    $layout = drupal_container()->get('plugin.manager.layout')->getDefinition($entity->layout);
    $provider_info = system_get_info($layout['provider']['type'], $layout['provider']['provider']);
    // Type can either be 'module' or 'theme'.
    $provider_text = t('@name @type', array('@name' => $provider_info['name'], '@type' => t($layout['provider']['type'])));
    $row['template'] = check_plain($layout['title'] . ' (' . $provider_text . ')');
    $operations = $this->buildOperations($entity);
    $row['operations']['data'] = $operations;
    return $row;
  }

}
