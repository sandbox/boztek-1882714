/**
 * @file
 * Theme functions for the layout js-app.
 */
(function ($) {
  /**
   * Theme function for a region.
   * @param id
   * @param label
   * @return {String}
   */
  Drupal.theme.layoutRegion = function (id, label, attributes) {
    var html =
      '<div id="layout-region-' + id + '" class="layout-region">' +
        '<div class="lining">' +
          '<header class="clearfix">' +
            '<div role="form" class="operations">' +
              '<button name="block" value="add" role="button" aria-label="' + Drupal.t('Add block to region') + '" class="icon icon-plus">' + Drupal.t('Add') + '</button>' +
              '<button name="block" value="configure" role="button" aria-label="' + Drupal.t('Configure block in region') + '" class="icon icon-gear">' + Drupal.t('Configure') + '</button>' +
            '</div>' +
            '<div class="info"><span class="label">' + label + '</span></div>' +
          '</header>' +
          '<div class="blocks">' +
            '<div class="row"></div>'
          '</div>' +
        '</div>' +
      '</div>';
    return html;
  }

  /**
   * Theme function to get the html for a block instance.
   * @param id
   * @param label
   * @return {String}
   */
  Drupal.theme.layoutBlock = function (id, label, attributes) {
    return '<div class="block" id="block-' + id + '">' +
      '<div class="lining">' +
        '<div class="info">' +
          '<span class="type-indicator mb-text">M</span>' +
          '<span class="label mb-text">' + label + '&nbsp;block</span>' +
        '</div>' +
        '<div class="operations mb-block-operations">' +
          '<a id="blockInstanceConfigure-' + id + '" class="gear mb-gear ajax" href="' + attributes.configureURL + '"></a>' +
          // this is only for demonstration purpose and so that we can have full blockInstance-CRUD.
          '<button name="block" value="delete" role="button" aria-label="' + Drupal.t('Delete') + '">' + Drupal.t('-') + '</button>'
        '</div>' +
      '</div>' +
     '</div>';
  };

})(jQuery);
