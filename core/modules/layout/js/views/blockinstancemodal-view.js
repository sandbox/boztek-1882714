/**
 * @file
 * This view controls and instantiates the dialog configuring a BlockInstance.
 */
(function ($, _, Backbone, Drupal) {

  "use strict";

  Drupal.layout = Drupal.layout || {};
  Drupal.layout.BlockInstanceModalView = Drupal.layout.ModalView.extend({
    events: {
      'click button.delete': 'removeBlockInstance'
    },
    removeBlockInstance: function() {
      // Remove model.
      this.model.collection.remove(this.model);
      // Destroy model on server.
      this.model.destroy();
      // Close and remove dialog.
      this.remove();
    },
    render: function() {
      this.$el.html(
        '<button class="delete">' + Drupal.t('Ok, delete!') + '</button> ' +
        '<button class="dialog-cancel">Cancel</button>'
      );
      this.show();
      return this;
    }
  });

})(jQuery, _, Backbone, Drupal);


