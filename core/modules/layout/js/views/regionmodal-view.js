/**
 * @file
 */
(function ($, _, Backbone, Drupal) {

  "use strict";

  Drupal.layout = Drupal.layout || {};
  Drupal.layout.RegionModalView = Drupal.layout.ModalView.extend({
    events: {
    },
    render: function() {
      this.$el.html(
        '<div>Configure region type? Load some FAPI form here.</div>' +
        '<button class="dialog-cancel">' + Drupal.t('Close') + '</button>'
      );
      this.show();
      return this;
    }
  });

})(jQuery, _, Backbone, Drupal);

