/**
 * @file
 * This view controls a region and the blockInstances contained in it. Opens
 * the BlockSelectorModalView on request.
 */
(function ($, _, Backbone, Drupal) {

  "use strict";

  Drupal.layout = Drupal.layout || {};

  Drupal.layout.RegionView = Backbone.View.extend({
    events:{
      'click [name="block"][value="add"]':'onClickAdd',
      'click [name="block"][value="configure"]':'onClickConfigure',
      'reorder':'reorderInstances'
    },

    onClickConfigure:function (e) {
      this.dialogView = new Drupal.layout.RegionModalView({
        model: this.model,
        title: this.model.get('label')
      });
      this.dialogView.render();
      e.preventDefault();
      e.stopPropagation();
    },

    onClickAdd:function (e) {
      var collection = Drupal.layout.getBlocksCollection();
      this.modalView = new Drupal.layout.BlockSelectorModalView({
        model: this.model,
        collection: collection,
        title: Drupal.t('Please select a block to place in the regions %region', {'%region': this.model.get('label')})
      });
      this.modalView.render();
      e.preventDefault();
      e.stopPropagation();
      return ;
    },

    // @todo: listen to collection events in app-view.js instead/propagate event.
    saveFullLayout: function() {
      // Show the "changed" notice.
      $('.display-changed').removeClass('js-hide');
      Drupal.layout.appModel.save();
    },

    initialize:function () {
      var blockInstances = this.model.get('blockInstances');
      this._collectionView = new Drupal.layout.UpdatingCollectionView({
        collection:blockInstances,
        nestedViewConstructor:Drupal.layout.BlockInstanceView,
        nestedViewTagName:'div',
        el: this.$el,
        nestedViewContainerSelector: '.blocks .row'
      });

      // @todo: be more selective about what changes trigger requests to the
      // server. And let that bubble up to the app-view or only persist the
      // region-specific changes here.
      blockInstances.on('reorder', this.saveFullLayout, this);
      blockInstances.on('add', this.saveFullLayout, this);
      blockInstances.on('remove', this.saveFullLayout, this);
    },

    render:function () {
      this.$el.html(Drupal.theme.layoutRegion(this.model.get('id'), this.model.get('label'), this.model.toJSON()));
      this._collectionView.render();
      // Making the whole layout-region-element sortable provides a larger area
      // to drop block instances on and allows for dropping on empty regions.
      this.$('.layout-region').sortable({
        items: '.block',
        connectWith: '.layout-region',
        cursor: 'move',
        stop: function(event, ui) {
          ui.item.trigger('drop', ui.item.index());
        }
      });
      return this;
    },

    remove:function () {
      this.$el.sortable('destroy');
      this.$el.empty();
      this._collectionView.remove();
    },

    reorderInstances:function (event, model, position) {
      var collection = this.model.get('blockInstances');
      var originCollection;
      // Handle cross-collection drag and drop.
      if (!collection.contains(model)) {
        originCollection = model.collection;
        // Let's remove it from the other first before adding it here.
        model.collection.remove(model, {silent: true});
        // This is set to silent to avoid potential race condition.
        originCollection.reorder({silent: true});
      } else {
        // We'll be re-adding immediately, so no need for rapid-fire events.
        collection.remove(model, {silent: true});
      }
      collection.add(model, {at:position});
      this.render();
    }
  });

})(jQuery, _, Backbone, Drupal);
