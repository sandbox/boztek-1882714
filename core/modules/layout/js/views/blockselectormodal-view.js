/**
 * @file
 * BlockSelectorModalView displays Blocks available for placement in a given
 * region, creates BlockInstance from a selected Block adds it to the region.
 *
 * @todo: use a form/server-side generated listing or a proper webservice to
 * retrieve the "placeable" Blocks.
 * @todo: split of Drupal.layout.BlockListItemView into separate file.
 */
(function ($, _, Backbone, Drupal) {

  "use strict";

  Drupal.layout = Drupal.layout || {};
  Drupal.layout.BlockSelectorModalView = Drupal.layout.ModalView.extend({
    events: {
      'select': 'selectBlock'
    },
    selectBlock: function(e, block) {
      // Model is RegionModel
      var instance = block.createBlockInstance();
      // Append at the end.
      instance.set('weight', this.model.get('blockInstances').length);
      this.model.get('blockInstances').add(instance);
      // Remove & close dialog.
      this.remove();
    },
    tagName: 'ul',
    render: function() {
      this.$el.empty();
      // @todo: refactor this to use nestedViewContainerSelector.
      this._collectionView = new Drupal.layout.UpdatingCollectionView({
        collection:this.collection,
        nestedViewConstructor:Drupal.layout.BlockListItemView,
        nestedViewTagName:'li'
      });
      this._collectionView.setElement(this.$el);
      this._collectionView.render();
      this.show();
      return this;
    },
    remove: function() {
      if (this._collectionView) {
        this._collectionView.remove();
      }
      // Apparently no need to call this.dialog.close(); remove this.$el
      // closes the jQueryUI Dialog, oh jqueryui magic ...
      this.$el.remove();
    }
  });

  Drupal.layout.BlockListItemView = Backbone.View.extend({
    events: {
      'click a': 'selectBlock'
    },
    selectBlock: function(e) {
      // Pass this click & model to the parent view.
      this.$el.trigger('select', [this.model]);
      e.preventDefault();
      e.stopPropagation();
      return ;
    },
    render: function() {
      this.$el.html('Add <a href="#">Block ' + this.model.get('label') + '</a>');
      return this;
    }
  });

})(jQuery, _, Backbone, Drupal);
