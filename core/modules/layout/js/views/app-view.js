/**
 * @file
 * This file holds the master view for the layout js app.
 */
(function ($, _, Backbone, Drupal) {

  "use strict";

  // @todo: be RESTful. Not sure whether the router/rest.module are in place.
  // Instead of using REST verbs, POST will be used and $_POST['_method'] will
  // contain the verb.
  Backbone.emulateHTTP = true;
  // Payloads will be sent as application/x-www-form-urlencoded, in
  // $_POST['model'].
  Backbone.emulateJSON = true;

  Drupal.layout = Drupal.layout || {};
  Drupal.layout.AppView = Backbone.View.extend({
    initializeRegions: function() {
      this.regionsView = new Drupal.layout.UpdatingCollectionView({
        el: this.$el.find('.layout-display'),
        collection: this.model.get('regions'),
        nestedViewConstructor:Drupal.layout.RegionView,
        nestedViewTagName:'div'
      });
    },
    initialize: function(options) {
      this.initializeRegions();
      // Listen to changes of the layout-property for a complete repaint.
      this.model.on('change:layout', function(m) {
        this.remove();
        // Reinitialize region - @todo: find a way of doing this w/o
        // reinitializing the view.
        this.initializeRegions();
        this.render();
      }, this);
    },
    render: function() {
      // @todo: this should move to layout.admin.js and provide better handling.
      // Do not setup the js app if another user is currently operating on this
      // layout (locked on the server via TempStore).
      if (this.options.locked) {
        return false;
      }
      this.regionsView.render();
      return this;
    },
    remove: function() {
      this.regionsView.remove();
    }
  });

})(jQuery, _, Backbone, Drupal);
