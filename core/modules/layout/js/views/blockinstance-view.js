/**
 * @file
 * This view controls a single BlockInstance.
 */
(function ($, _, Backbone, Drupal) {

  "use strict";

  Drupal.layout = Drupal.layout || {};

  Drupal.layout.BlockInstanceView = Backbone.View.extend({
    events:{
      'click [name="block"][value="delete"]': 'onClickDelete',
      'drop':'onDrop'
    },
    initialize: function() {
      this.model.on('change', this.render, this);
    },
    onDrop:function (event, index) {
      // Trigger reorder, will be handled in Drupal.layout.RegionView.
      this.$el.trigger('reorder', [this.model, index]);
      event.preventDefault();
      event.stopPropagation();
      return ;
    },
    onClickDelete:function (event) {
      this.dialogView = new Drupal.layout.BlockInstanceModalView({
        model: this.model,
        title: this.model.get('label')
      });
      this.dialogView.render();
      event.preventDefault();
      event.stopPropagation();
      return ;
    },
    getConfigureURL: function() {
      return Drupal.url('admin/structure/layouts/manage/' + drupalSettings.layout.id + '/blockinstances/' + this.model.get('id') + '/configure' );
    },
    render:function () {
      // Remove any existent Drupal.ajax.
      Drupal.layout.deajaxify(this.$el);
      // If you want to have the template render the "top" element of your view
      // you need to do this.
      // @see http://stackoverflow.com/questions/11594961/backbone-not-this-el-wrapping
      var old = this.$el;
      this.setElement(Drupal.theme('layoutBlock', this.model.get('id'), this.model.get('label'), {
        'configureURL': this.getConfigureURL()
      }));
      old.replaceWith(this.$el);
      // Rewire Drupal.ajax.
      Drupal.layout.ajaxify(this.$el);
      return this;
    },
    remove: function() {
      Drupal.layout.deajaxify(this.$el);
      this.$el.remove();
    }
  });


})(jQuery, _, Backbone, Drupal);
