(function ($, window, Drupal, drupalSettings) {

"use strict";

var appView;

Drupal.ajax.prototype.commands.layoutBlockReload = function (ajax, response, status) {
  // Find the appropriate model by its id.
  var m = Drupal.layout.getBlockInstanceModelById(response.data.id);
  if (m) {
    // The views will take care of all necessary updates (unbinding, rebinding).
    m.set(response.data.info);
  }
};

/**
 * Attach display editor functionality.
 */
Drupal.behaviors.displayEditor = {
  attach: function (context, settings) {
    function randomId() {
      var chars = "abcdefghiklmnopqrstuvwxyz";
      var randomString = '';
      for (var i=0; i < 8; i++) {
        var rnum = Math.floor(Math.random() * chars.length);
        randomString += chars.substring(rnum,rnum+1);
      }
      return randomString;
    }

    /**
     * Bind links to open in a dialog using Drupal.ajax.
     * @param el
     */
    Drupal.layout.ajaxify = function(el) {
      // Bind Ajax behaviors to all items showing the class.
      $(el).find('.ajax').once('ajax', function () {
        var element_settings = {};
        element_settings.progress = { 'type': null };
        element_settings.dialog = {
          modal: true
        };

        if ($(this).attr('href')) {
          element_settings.url = $(this).attr('href');
          element_settings.event = 'click.layout';
        }
        var base = $(this).attr('id');
        Drupal.ajax[base] = new Drupal.ajax(base, this, element_settings);
      });
    }

    /**
     * Unbind links bound to Drupal.ajax.
     * @param el
     */
    Drupal.layout.deajaxify = function(el) {
      $(el).find('.ajax').each(function() {
        var base = $(this).attr('id');
        if (Drupal.ajax[base]) {
          $(this).off('click.layout');
          delete Drupal.ajax[base]
        }
      });
    }

    /**
     * Retrieve the BlockInstanceModel
     * @param id
     * @return {*}
     */
    Drupal.layout.getBlockInstanceModelById = function(id) {
      var m;
      Drupal.layout.appModel.get('regions').each(function(region) {
        m = m || region.get('blockInstances').get(id);
      }, this);
      return m;
    }

    /**
     * Helper function generating a BlocksCollection populated with randomly
     * named items.
     *
     * @return {Drupal.layout.BlocksCollection}
     */
    Drupal.layout.getBlocksCollection = function() {
      var blocks = [];
      // Generate a bunch of randomly named blocks.
      for (var i = 0; i<10; i++) {
        var id = randomId();
        var b = new Drupal.layout.BlockModel({
          'id': id,
          'label': 'Label ' + id
        });
        blocks.push(b);
      }
      return new Drupal.layout.BlocksCollection(blocks);
    }

    /**
     * Generates the required Backbone Collections and Models.
     * @param layoutData
     * @return {Drupal.layout.RegionsCollection}
     */
    function generateRegionCollections(layoutData) {
      var regions = new Drupal.layout.RegionsCollection();
      _(layoutData.regions).each(function(region) {
        regions.add(new Drupal.layout.RegionModel({
          id: region.id,
          label: region.label,
          blockInstances:
            new Drupal.layout.BlockInstancesCollection().reset(region.blockInstances)
        }));
      });
      return regions;
    }

    // Initial attaching.
    if (!appView) {
      Drupal.layout.appModel = new Drupal.layout.AppModel({
        id: drupalSettings.layout.id,
        layout: drupalSettings.layout.layoutData.layout,
        regions: generateRegionCollections(drupalSettings.layout.layoutData)
      });
      appView = new Drupal.layout.AppView({
        model: Drupal.layout.appModel,
        el: $('#block-system-main'),
        locked: drupalSettings.layout.locked
      });
      // @todo: we need to do this in order to circumvent the merge-behavior of
      // Drupal.ajax on drupalSettings (which makes sense, just not here).
      drupalSettings.layout.layoutData = {};
      appView.render();
    } else {
      // Drupal.ajax has (good) reasons to call the attach function three times
      // per response (triggered by layout select menu). But we
      // need this only once and we need to make sure that the layout data is
      // replaced not merged, that's why we do this stunt. There needs to be
      // some form of making this less awkward.
      if (drupalSettings.layout.layoutData.id) {
        // Updating the model will trigger an rendering as appropriate.
        Drupal.layout.appModel.set({
          id: drupalSettings.layout.id,
          layout: drupalSettings.layout.layoutData.layout,
          regions: generateRegionCollections(drupalSettings.layout.layoutData)
        });
        // @todo: we need to do this in order to circumvent the merge-behavior of
        // Drupal.ajax on drupalSettings (which makes sense, just not here).
        drupalSettings.layout.layoutData = {};
      }
    }

  }
};

})(jQuery, window, Drupal, drupalSettings);
