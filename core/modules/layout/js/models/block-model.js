/**
 * @file
 * This model corresponds to a block that can be placed
 */
(function ($, _, Backbone, Drupal, drupalSettings) {
  "use strict";

  Drupal.layout = Drupal.layout || {};

  Drupal.layout.BlockModel = Backbone.Model.extend({
    defaults: {
      /* CMI name */
      'id': null,
      'label': '',
      'description': '',
      'config': {}
    },
    // @todo: create a new BlockInstanceModel instance via a webservice (or
    // another way of ensuring that the id of the BlockInstanceMdoel is unique).
    createBlockInstance: function() {
      return new Drupal.layout.BlockInstanceModel({
        'id': this.get('id'),
        'label': this.get('id'),
        'blockId': this.get('id'),
        'config': this.get('config')
      });
    }
  });

})(jQuery, _, Backbone, Drupal, drupalSettings);
